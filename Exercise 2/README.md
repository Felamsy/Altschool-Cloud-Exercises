# 10 LINUX COMMAND

Linux is a family of open-source Unix-like operating systems based on the Linux kernel. The Linux command is a utility of the Linux operating system. All basic and advanced tasks can be done by executing commands. The commands are executed on the Linux terminal. The terminal is a command-line interface to interact with the system, which is similar to the command prompt in the Windows OS. Commands in Linux are case-sensitive.

## Linux system management Command

1. systemctl Command

The systemctl command is a utility which is responsible for examining and controlling the systemd system and service manager. It is a collection of system management libraries, utilities and daemons which function as a successor to the System V init daemon. The new systemctl commands have proven quite useful in managing a servers services

syntax

$ systemctl status apache2

output

![systemctl Command](https://github.com/Felamsy/Altschool-Cloud-Exercises/blob/main/Exercise%202/images/COMMAND.png?raw=true)

## Linux I/O Redirection

2. < > Command
   Redirection can be defined as changing the way from where commands read input to where commands sends output. You can redirect input and output of a command. For redirection, meta characters are used. Redirection can be into a file (shell meta characters are angle brackets '<', '>') or a program ( shell meta characters are pipesymbol '|').

syntax

$ ls > text.txt

output
![<> command](https://github.com/Felamsy/Altschool-Cloud-Exercises/blob/main/Exercise%202/images/COMMAND.png?raw=true)

## Linux System Admin Command

3. users Command
   Show current logged in users

syntax

$ users

output
![user](https://github.com/Felamsy/Altschool-Cloud-Exercises/blob/main/Exercise%202/images/user.png?raw=true)

4. su Command
   Switch from one to another user

syntax

$ sudo su

output
![su](https://github.com/Felamsy/Altschool-Cloud-Exercises/blob/main/Exercise%202/images/sudo%20su.png?raw=true)

## Basic Unix Tools

5. date Command
   The date command is basic utility, and it can be used by executing without any argument. It will display the current date and time. Consider the below command

syntax

$ date

output
![date](https://github.com/Felamsy/Altschool-Cloud-Exercises/blob/main/Exercise%202/images/date.png?raw=true) 6. cal command

cal command is a calendar command in Linux which is used to see the calendar of a specific month or a whole year. The rectangular bracket means it is optional, so if used without an option, it will display a calendar of the current month and year

syntax

$ cal

output
![cal](https://github.com/Felamsy/Altschool-Cloud-Exercises/blob/main/Exercise%202/images/calendar.png?raw=true)

## Environment Variables command

7. env command

env is a shell command for Unix and Unix-like operating systems. It is used to either print a list of environment variables or run another utility in an altered environment without having to modify the currently existing environment.
syntax

$ env

output
![env](https://github.com/Felamsy/Altschool-Cloud-Exercises/blob/main/Exercise%202/images/env%20Display%20all%20environment%20variables.png?raw=true)

## PERFORMANCE MONITORING AND STATISTICS

8. lsof command

lsof command stands for List Of Open File. This command provides a list of files that are opened. Basically, it gives the information to find out the files which are opened by which process. With one go it lists out all open files in output console.

syntax

$ lsof

output
![lsof](https://github.com/Felamsy/Altschool-Cloud-Exercises/blob/main/Exercise%202/images/List%20all%20open%20files%20on%20the%20system%20LSOF.png?raw=true)

9. free command

The Linux free command outputs a summary of RAM usage, including total, used, free, shared, and available memory and swap space. The command helps monitor resource usage and allows an admin to determine if there's enough room for running new programs. In this tutorial, you will learn to use the free command in Linux

syntax

$ free

output
![free](https://github.com/Felamsy/Altschool-Cloud-Exercises/blob/main/Exercise%202/images/free%20Gives%20free%20RAM%20on%20your%20system.PNG?raw=true)

## Linux File Ownership

10. chmod Command
    Command chown is used to change the owner of the file.

syntax

$ chmod u+x file

output
![chmod u+x file](https://github.com/Felamsy/Altschool-Cloud-Exercises/blob/main/Exercise%202/images/chmod.png?raw=true)
