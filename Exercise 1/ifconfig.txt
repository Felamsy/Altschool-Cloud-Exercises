enp0s3: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 10.0.2.15  netmask 255.255.255.0  broadcast 10.0.2.255
        inet6 fe80::97:75ff:fe78:6272  prefixlen 64  scopeid 0x20<link>
        ether 02:97:75:78:62:72  txqueuelen 1000  (Ethernet)
        RX packets 1301  bytes 371848 (371.8 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 1005  bytes 148523 (148.5 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

enp0s8: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.56.3  netmask 255.255.255.0  broadcast 192.168.56.255
        inet6 fe80::a00:27ff:fe63:3a87  prefixlen 64  scopeid 0x20<link>
        ether 08:00:27:63:3a:87  txqueuelen 1000  (Ethernet)
        RX packets 298  bytes 60106 (60.1 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 159  bytes 28894 (28.8 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 69  bytes 6046 (6.0 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 69  bytes 6046 (6.0 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0